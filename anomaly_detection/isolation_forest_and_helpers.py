from matplotlib import pyplot as plt
import pandas as pd
from pandas import DataFrame
from sklearn.ensemble import IsolationForest
import seaborn as sns
import numpy as np
import shap

from constants import c_enum_id


def combine_survey_with_enumerator_features(
    surveys: DataFrame, enumerators: DataFrame, feature_names
):
    # Join by enumerator
    surveys_and_enumerators = surveys.merge(
        enumerators, how="left", left_on=c_enum_id, right_index=True
    )

    # Get list of which enumerator did each survey
    enumerator_list = surveys_and_enumerators[c_enum_id]

    # Extract just the desired features
    features = surveys_and_enumerators[feature_names]

    return features, enumerator_list


def isolation_forest(
    features: pd.DataFrame, enumerator_list, num_trees=1000, threshold=-0.5
):
    """Perform Isolation Forest on desired features
            features: DataFrame of features for each survey
            num_trees: number of trees to fit in isolation forest
            enumerator_list: list of enumerators for each survey
            threshold: 
                float that determines when a DataPoint is an outlier
                default is -0.5 and values smaller than that (more negative) are
                more anomalous. In practice, -0.5 should be the largest threshold used
        
        
        Returns:
            IF_features: DataFrame with all features, isolation scores, and whether
            point is outlier according to Isolation Forest
    """

    IF = IsolationForest(n_estimators=num_trees)
    IF.fit(features)

    survey_features = features.join(enumerator_list)

    survey_features["score IF"] = IF.score_samples(features)
    survey_features["survey_type"] = (survey_features["score IF"] < threshold).astype(
        int
    )
    survey_features["survey_type"] = np.where(
        survey_features["survey_type"] == 1, "anomaly", "typical"
    )

    return survey_features, IF



def shap_intepretation(
    features: DataFrame, surveys_with_IF_scores: DataFrame, IF: IsolationForest
):
    explainer = shap.TreeExplainer(IF)
    shap_values = explainer.shap_values(features)  # Calculate SHAP values
    shap.initjs()

    #shap.summary_plot(shap_values, features)
    #shap.summary_plot(shap_values, features, plot_type="bar")

    # Add shap values to data frame
    all_features_shap_values = DataFrame(shap_values, columns=features.columns)
    first_important = all_features_shap_values.idxmin(axis=1)

    second_important = all_features_shap_values.T.apply(
        lambda x: x.nsmallest(2).idxmax()
    )

    third_important = all_features_shap_values.T.apply(
        lambda x: x.nsmallest(3).idxmax()
    )

    all_features_shap_values["first_important_feature"] = first_important
    all_features_shap_values["second_important_feature"] = second_important
    all_features_shap_values["third_important_feature"] = third_important
    all_features_shap_values.index = surveys_with_IF_scores.index

    return all_features_shap_values, shap_values


