from pandas import DataFrame

from constants import (
    c_enum_id,
    c_start_date,
    c_enum_sec_on_surveys_that_day,
    c_active_seconds_spent,
)


def attach_survey_start_aggregates(enumerators: DataFrame, surveys: DataFrame):
    """:return enumerators with `survey_start_std` (standard deviation of hour
    that enumerator starts surveys), `max_survey_start_span` (largest in-day difference
    between first and last hour that enumerator starts a survey), `median_survey_start_span`
    (like max_survey_start_span, but median instead of max), and `num_surveys` (number of surveys by enumerator)."""
    enumerators["survey_start_std"] = surveys.groupby(c_enum_id).start.apply(
        lambda x: x.dt.hour.std()
    )
    enumerators["max_survey_start_span"] = surveys.groupby(c_enum_id).start.apply(
        lambda x: x.dt.hour.max() - x.dt.hour.min()
    )
    enumerators["median_survey_start_span"] = (
        surveys.groupby([c_enum_id, c_start_date])
        .start.apply(lambda x: x.dt.hour.max() - x.dt.hour.min())
        .groupby(level=c_enum_id)
        .median()
    )
    enumerators["num_surveys"] = surveys.groupby(c_enum_id).start.count()
    return enumerators


def attach_median_active_seconds_per_day(enumerators: DataFrame, surveys: DataFrame):
    """:return enumerators with `median_active_seconds_per_day` (median seconds per day
    that enumerator actively spends on filling out surveys)."""
    enumerators["median_active_seconds_per_day"] = surveys.groupby(c_enum_id)[
        c_enum_sec_on_surveys_that_day
    ].median()
    return enumerators


def attach_median_active_seconds_per_survey(enumerators: DataFrame, surveys: DataFrame):
    """:return enumerators with `median_seconds_per_day` (median seconds per survey
    that enumerator actively spends filling it out)."""
    enumerators["median_active_seconds_per_survey"] = surveys.groupby(c_enum_id)[
        c_active_seconds_spent
    ].median()
    return enumerators
