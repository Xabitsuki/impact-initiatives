from pandas import DataFrame

from constants import c_deindexed_median_seconds, c_seconds, c_deindexed_node, c_node, c_mean_dur, c_dur


def attach_typical_durations(
    audits: DataFrame, nodes: DataFrame, deindexed_nodes: DataFrame
):
    """:return `deindexed_nodes` with deindexed_median_seconds feature."""
    deindexed_nodes[c_deindexed_median_seconds] = (
        audits.join(nodes, on=c_node).groupby(c_deindexed_node)[c_seconds].median()
    )
    deindexed_nodes[c_mean_dur] = (
        audits.join(nodes, on=c_node).groupby(c_deindexed_node)[c_dur].mean()
    )
    return deindexed_nodes


def attach_event(audits: DataFrame, nodes: DataFrame, deindexed_nodes: DataFrame):
    """:return `deindexed_nodes` with `event` column collected from `audits`."""
    deindexed_nodes["event"] = (
        audits.join(nodes, on=c_node)
        .groupby(c_deindexed_node)
        .apply(lambda df: df.event.iloc[0])
        .to_frame("event")
    )
    return deindexed_nodes
