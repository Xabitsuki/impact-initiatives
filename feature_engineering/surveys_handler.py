import numpy as np
import pandas as pd
from pandas import DataFrame, Series

from constants import (
    c_deindexed_median_seconds,
    c_seconds,
    c_relative_pace,
    c_deindexed_node,
    c_node,
    c_uuid,
    c_enum_id,
    c_start_date,
    c_enum_sec_on_surveys_that_day,
    c_active_seconds_spent,
    c_unique_question_duration_correlation,
    c_active_fraction_before_long_break,
    c_largest_relative_pace_increase,
    c_dur,
    c_mean_dur,
    c_median_log_relative_pace,
)


def attach_time_correlations(
    surveys: DataFrame, audits: DataFrame, nodes: DataFrame, deindexed_nodes: DataFrame
):
    """:return surveys with `deindexed_node_duration_correlation` (correlation between node times
    in this survey and median question times on those questions, when index is ignored) and
    `indexfree_question_duration_correlation` (same, but only looking at questions and group
    questions that have no index), in both cases only counting the first occurence of each question."""
    anomaly_quantile = 0.025
    usual_audits = _without_outliers(audits, c_dur, anomaly_quantile)
    usual_deindexed_nodes = _without_outliers(
        deindexed_nodes, c_mean_dur, anomaly_quantile
    )
    usual_audits_with_node_info = usual_audits.join(nodes, on=c_node).join(
        usual_deindexed_nodes, on=c_deindexed_node, rsuffix="_deindexed", how="inner"
    )
    questions_and_group_questions_first_occurence = usual_audits_with_node_info[
        (
            (usual_audits_with_node_info.event == "question")
            | (usual_audits_with_node_info.event == "group questions")
        )
    ].drop_duplicates([c_deindexed_node, c_uuid])
    surveys[
        c_unique_question_duration_correlation
    ] = _compute_duration_median_correlation(
        questions_and_group_questions_first_occurence
    )
    return surveys


def _compute_duration_median_correlation(joined_df: DataFrame):
    """:return correlation between log seconds and log deindexed_median_seconds
    (log of these features has more normal distribution, thus correlation becomes more stable)."""
    return joined_df.groupby(c_uuid).apply(
        lambda group_df: group_df[c_dur].corr(group_df[c_mean_dur])
    )


def _without_outliers(df: DataFrame, col_name: str, quantile: float):
    """:return df without rows where s[col_name] is above the `quantile` quantile
    (0.90 found to be a good value for catching anomalies)."""
    lower_cutoff = df[col_name].quantile(quantile)
    upper_cutoff = df[col_name].quantile(1 - quantile)
    return df[df[col_name].between(lower_cutoff, upper_cutoff)]


def attach_median_log_relative_pace(surveys: DataFrame, audits: DataFrame):
    surveys[c_median_log_relative_pace] = surveys[
        c_median_log_relative_pace
    ] = audits.groupby(c_uuid)[c_relative_pace].apply(
        lambda s: np.median(np.log(s.dropna()))
    )
    return surveys


def attach_log_longer_shorter_median_pace_ratio(
    surveys: DataFrame, audits: DataFrame, nodes: DataFrame, deindexed_nodes: DataFrame
):
    """:return surveys with `log_longer_shorter_median_pace_ratio` (log of median relative_pace on
    questions with highest 5% median response time divided by the median relative_pace of the rest)"""
    joined = audits.join(nodes, on=c_node).join(
        deindexed_nodes, on=c_deindexed_node, rsuffix="_"
    )

    questions = deindexed_nodes[deindexed_nodes.event == "question"]
    # Worthwhile question: Would it be better to pick these on a
    #  per-survey basis (among questions asked in that survey)?
    cutoff = questions[c_deindexed_median_seconds].quantile(0.95)
    long_questions = questions[questions[c_deindexed_median_seconds] > cutoff].index
    audits_on_long_questions = joined[joined[c_deindexed_node].isin(long_questions)]
    audits_on_shorter_questions = joined[~joined[c_deindexed_node].isin(long_questions)]
    median_pace_on_longer = audits_on_long_questions.groupby(c_uuid)[
        c_relative_pace
    ].median()
    median_pace_on_shorter = audits_on_shorter_questions.groupby(c_uuid)[
        c_relative_pace
    ].median()
    surveys["log_longer_shorter_median_pace_ratio"] = np.log(
        median_pace_on_longer / median_pace_on_shorter
    )
    return surveys


def attach_active_seconds_spent(surveys: DataFrame, audits: DataFrame):
    """:return surveys with `active_seconds_spent` (for how many seconds the
    enumerator was actively engaging with this survey)."""
    surveys[c_active_seconds_spent] = (
        audits[audits.seconds < 900].groupby(c_uuid)[c_seconds].sum()
    )
    return surveys


def attach_active_seconds_that_day(surveys: DataFrame):
    """:return `surveys` with `enum_sec_on_surveys_that_day` column
    (how many seconds the enumerator actively spent on surveys the day
    this survey was started)."""
    seconds_of_date_spent = (
        surveys.groupby([c_enum_id, c_start_date])[c_active_seconds_spent]
        .sum()
        .rename(c_enum_sec_on_surveys_that_day)
    )
    surveys = surveys.join(seconds_of_date_spent, on=[c_enum_id, c_start_date])
    return surveys


def attach_start_date(surveys: DataFrame):
    """surveys with `start_date` (the date of the DateTime in the `start` column)."""
    surveys[c_start_date] = surveys.start.dt.date
    return surveys


def _extract_active_fraction_before_long_break(
    audits_group: DataFrame, surveys: DataFrame
):
    """:return active_fraction_before_long_break for this particular `audits_group` (audits of one survey)"""
    uuid = audits_group[c_uuid].iloc[0]
    active_seconds_before_first_long_break = 0
    # Not ideal to loop over rows, but small enough dataframes that it hardly matters (<2s over all groups)
    for seconds in audits_group[c_seconds]:
        if seconds < 60 * 15:
            active_seconds_before_first_long_break += seconds
        elif seconds > 60 * 15:
            break
    total_active_seconds = surveys.loc[uuid][c_active_seconds_spent]
    return active_seconds_before_first_long_break / total_active_seconds


def attach_active_fraction_before_long_break(surveys: DataFrame, audits: DataFrame):
    """:return surveys with `active_fraction_before_long_break` (fraction of active
    seconds spent on this survey that occured before its first long break (30 min)."""
    surveys[c_active_fraction_before_long_break] = audits.groupby(c_uuid).apply(
        _extract_active_fraction_before_long_break, surveys
    )
    return surveys


def _with_largest_relative_pace_increase(joined_group: DataFrame):
    first_encounters = joined_group.drop_duplicates(c_deindexed_node, keep="first")
    buffer = len(first_encounters) // 4
    relative_pace = first_encounters[c_relative_pace]
    median_until_now = relative_pace.expanding(buffer).median()
    median_from_now = relative_pace[::-1].expanding(buffer).median()[::-1]
    relative_relative_pace_increase = median_from_now / median_until_now
    return relative_relative_pace_increase.max()


def attach_largest_relative_pace_increase(
    surveys: DataFrame, audits: DataFrame, nodes: DataFrame
):
    """surveys with `largest_relative_pace_increase` (largest relative change in median pace between first
    n audits of survey and remaining len-n audits of survey, only counting first encounter per deindexed node)"""
    surveys[c_largest_relative_pace_increase] = (
        audits.join(nodes, on=c_node)
        .groupby(c_uuid)
        .apply(_with_largest_relative_pace_increase)
    )
    return surveys


def attach_duration_and_time_per_question(surveys: DataFrame, audits: DataFrame):
    """ Calculates Total Duration of Survey (in minutes), ignoring times when the survey was paused 
        until the time it was resumed.
    """

    # The following is the previous code, which is slightly more understandable but much slower:

    # duration = (audit.iloc[-1].loc["start"] - audit.iloc[0].loc["start"]) / 60000
    # resume = int("form resume" in audit["event"].unique())

    # timeGap = 0
    # if resume:
    #    indices = audit.index[audit["event"] == "form resume"].tolist()

    #    for i in indices:
    #     gap = audit.loc[i, "start"] - audit.loc[i-1,"start"]
    #        timeGap += (gap / 60000)

    duration = audits.groupby(c_uuid).apply(
        lambda audit: audit.iloc[-1].loc["start"] - audit.iloc[0].loc["start"]
    )
    time_paused = audits.groupby(c_uuid).apply(
        lambda audit: audit.start.diff()[
            audit.index[audit["event"] == "form resume"].tolist()
        ].sum()
    )
    surveys["duration"] = (duration - time_paused) / 60000

    count_unique_questions = audits.groupby(c_uuid).apply(
        lambda x: len(x.node.unique())
    )
    surveys["num_questions"] = count_unique_questions
    surveys["time_per_question"] = surveys["duration"] / surveys["num_questions"]

    return surveys


def attach_variance(surveys: DataFrame, audits: DataFrame):
    """Calculates Variance in Question Response Times (in minutes)"""

    variances = audits.groupby(c_uuid).apply(
        lambda x: (
            (
                x[(x["event"] == "question") | (x["event"] == "group questions")].start
                - x[(x["event"] == "question") | (x["event"] == "group questions")].end
            )
            / 60000
        ).var(skipna=True)
    )
    surveys["variance"] = variances
    return surveys


def attach_constraint_note_count(
    surveys: DataFrame, audits: DataFrame, questionnaire: DataFrame
):
    """ Counts the number of constraint notes that pop up in survey
       surveys - DataFrame of surveys to append features onto
       audit - Individual Audit File
   """

    notes = questionnaire[questionnaire["type"] == "note"]
    constraint_notes = notes[~notes["relevant"].isna()]
    constraint_note_names = constraint_notes["name"]

    num_constraints = audits.groupby(c_uuid).apply(
        _count_constraint_notes, constraint_note_names
    )

    surveys["num_constraint_notes"] = num_constraints

    return surveys


def attach_constraint_backtrack(
    surveys: DataFrame, audits: DataFrame, questionnaire: DataFrame
):
    question_names = questionnaire["name"].fillna("NaN")
    constraints = questionnaire["constraint"]

    reference_matrix = []
    for names in question_names:
        references = list(
            constraints.str.contains("{" + names + "}").fillna(0).astype(int)
        )
        reference_matrix.append(references)

    # The row is the constrained question and the column is the constraining question.
    # So a value of 1 in the row "demo_number_hh" and the column "demo_nb_membre" means that
    # the "demo_number_hh" question comes after the "demo_nb_membre" question and is
    # constrained by it
    constraint_matrix = DataFrame(
        reference_matrix, columns=question_names, index=question_names
    ).transpose()
    constraint_matrix = constraint_matrix.loc[:, (constraint_matrix != 0).any(axis=0)]
    constraint_matrix = constraint_matrix.loc[(constraint_matrix != 0).any(axis=1), :]

    num_backtracks = audits.groupby(c_uuid).apply(
        _constraint_backtrack, constraint_matrix
    )

    surveys["constraint_backtracks"] = num_backtracks

    return surveys


def attach_form_resume_count(surveys: DataFrame, audits: DataFrame):
    """"Counts number of times the survey is paused and then later resumed"""
    resume_count = audits.groupby(c_uuid).apply(
        lambda x: x.event.str.count("form resume").sum()
    )

    surveys["resume_count"] = resume_count
    return surveys


def attach_constraint_error(surveys: DataFrame, audits: DataFrame):
    """"Counts number of times the audit event 'constraint error' is reached"""
    constraint_errors = audits.groupby(c_uuid).apply(
        lambda x: x.event.str.count("constraint error").sum()
    )

    surveys["constraint_errors"] = constraint_errors
    return surveys


def _constraint_backtrack(audit: DataFrame, constraint_matrix: DataFrame):
    """This function calculates the number of times the survey re-visits questions that place constraints
    on other questions. If this happens frequently it could be a sign of many incorrect values being entered
    and having to go back to reference or change the constraining value"""
    audit = audit[audit[c_node].notna()]

    audit[c_node] = audit[c_node].apply(lambda x: x.split("/")[-1])

    # Look only at nodes that are constrained or constraining questions
    constraint_audit = audit[
        (audit[c_node].isin(constraint_matrix.columns))
        | (audit[c_node].isin(constraint_matrix.index))
    ]

    # Now filter out questions that were only looked at for one second, this
    # implies that the question was just scrolled past on the way to another question
    # and wasn't referred to or edited.
    constraint_audit_final = constraint_audit[
        (constraint_audit.end - constraint_audit.start) > 1000
    ]
    constraint_audit_final = constraint_audit_final.reset_index()

    constraint_backtracks = 0
    audit_length = len(constraint_audit_final)
    for i, row in constraint_audit_final.iterrows():
        current_question = row[c_node]
        if current_question in constraint_matrix.index:
            ref_mat_row = constraint_matrix.loc[current_question]
            for j in range(i + 1, audit_length):
                ref_question = constraint_audit_final[c_node][j]
                if ref_question in ref_mat_row.index:
                    constraint_backtracks += ref_mat_row[ref_question]
    return constraint_backtracks


def _count_constraint_notes(audit: DataFrame, notes: Series):
    """ Counts the number of constraint notes that pop up in survey
        audit: Individual Audit File
        notes: series of note names to be checked for in audit file

        To generate notes, run following command:
                 notes = questionnaire[questionnaire["type"] == "note"]
                 notes = notes[~notes["relevant"].isna()]

        "notes" is created outside of this function to calculating it again for each audit file
    """
    num_notes = 0
    audit_questions = audit[c_node]
    for name in notes:
        num_notes += audit_questions.str.count(name).sum()

    return num_notes
