from pandas import DataFrame

from constants import c_median_seconds, c_seconds, c_node


def attach_median_seconds(audits: DataFrame, nodes: DataFrame):
    """:return `deindexed_nodes` with median_seconds feature."""
    nodes[c_median_seconds] = audits.groupby(c_node)[c_seconds].median()
    return nodes
