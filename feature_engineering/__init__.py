from pandas import DataFrame

from feature_engineering import enumerators_handler
from feature_engineering.audits_handler import attach_durations, attach_relative_pace
from feature_engineering.bootstrapper import initialize_dataframes
from feature_engineering.deindexed_nodes_handler import (
    attach_typical_durations,
    attach_event,
)
from feature_engineering.enumerators_handler import (
    attach_survey_start_aggregates,
    attach_median_active_seconds_per_day,
    attach_median_active_seconds_per_survey,
)
from feature_engineering.nodes_handler import attach_median_seconds
from feature_engineering.surveys_handler import (
    attach_time_correlations,
    attach_log_longer_shorter_median_pace_ratio,
    attach_active_seconds_that_day,
    attach_start_date,
    attach_active_seconds_spent,
    attach_duration_and_time_per_question,
    attach_variance,
    attach_constraint_note_count,
    attach_constraint_backtrack,
    attach_form_resume_count,
    attach_constraint_error,
    attach_active_fraction_before_long_break,
    attach_largest_relative_pace_increase, attach_median_log_relative_pace,
)
from storage_handler import persist_processed


def attach_features(
    enumerators: DataFrame,
    surveys: DataFrame,
    audits: DataFrame,
    nodes: DataFrame,
    deindexed_nodes: DataFrame,
    questionnaire: DataFrame,
):
    """Attach features to parameters (inplace) and return them."""
    # A fair amount of coupling between these functions, as each relies on DataFrame
    # created by function calls before it, but hard to do much about it without making
    # this attach_features function into a monster, since features are inherently dependent
    surveys = attach_start_date(surveys)
    enumerators = attach_survey_start_aggregates(enumerators, surveys)
    deindexed_nodes = attach_event(audits, nodes, deindexed_nodes)
    audits = attach_durations(audits)
    nodes = attach_median_seconds(audits, nodes)
    deindexed_nodes = attach_typical_durations(audits, nodes, deindexed_nodes)
    surveys = attach_time_correlations(
        surveys, audits, nodes, deindexed_nodes
    )
    audits = attach_relative_pace(audits, nodes, deindexed_nodes)
    surveys = attach_median_log_relative_pace(surveys, audits)
    surveys = attach_log_longer_shorter_median_pace_ratio(
        surveys, audits, nodes, deindexed_nodes
    )
    surveys = attach_active_seconds_spent(surveys, audits)
    surveys = attach_active_seconds_that_day(surveys)
    surveys = attach_active_fraction_before_long_break(surveys, audits)
    surveys = attach_largest_relative_pace_increase(surveys, audits, nodes)
    surveys = attach_duration_and_time_per_question(surveys, audits)
    surveys = attach_variance(surveys, audits)
    surveys = attach_constraint_note_count(surveys, audits, questionnaire)
    surveys = attach_constraint_backtrack(surveys, audits, questionnaire)
    surveys = attach_form_resume_count(surveys, audits)
    surveys = attach_constraint_error(surveys, audits)

    enumerators = attach_median_active_seconds_per_day(enumerators, surveys)
    enumerators = attach_median_active_seconds_per_survey(enumerators, surveys)

    persist_processed(enumerators, surveys, audits, nodes, deindexed_nodes)
    return enumerators, surveys, audits, nodes, deindexed_nodes


def make_featurized_dataframes():
    """:return enumerators, surveys, audits, nodes, deindexed_nodes, all with various features."""
    return attach_features(*initialize_dataframes())
