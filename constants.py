from pathlib import Path

c_enum_id = "global_enum_id"
c_uuid = "uuid"
c_start_date = "start_date"
c_seconds = "seconds"
c_dur = "sqrt_log_seconds"
c_median_seconds = "median_seconds"
c_mean_dur = "mean_sqrt_log_seconds"
c_deindexed_median_seconds = "deindexed_median_seconds"
c_relative_pace = "relative_pace"
c_median_log_relative_pace = "median_log_relative_pace"
c_node = "node"
c_deindexed_node = "deindexed_node"
c_active_seconds_spent = "active_seconds_spent"
c_enum_sec_on_surveys_that_day = "enum_active_sec_on_surveys_that_day"
c_unique_question_duration_correlation = "unique_question_duration_correlation"
c_active_fraction_before_long_break = "active_fraction_before_long_break"
c_largest_relative_pace_increase = "largest_relative_pace_increase"

# These values (with prefix `src_`) are specific to the survey / year / region
# and how the raw data is stored on your system, so modify them accordingly
src_data_folder = Path(r"..\data_to_share\audits to share\BurkinaFaso_2021")
src_surveys_path = (
    src_data_folder / "BFA_MSNA_21 - Données et journal de nettoyage.xlsx"
)
src_surveys_sheet = "Données brutes"
src_questionaire_path = src_data_folder / "BFA_MSNA_questionnaire.xlsx"
src_questionaire_sheet = "survey"
src_audits_zip = src_data_folder / "audit_files.zip"
src_audits_folder = (
    src_data_folder
    / r"audit_files\reach_global\attachments\9bf0250c6251407a9b9757f39ef8ba80"
)

data_folder = Path("./data")
raw_surveys_path = data_folder / "raw_surveys.parquet"
raw_audits_folder = data_folder / "raw_audits"
results_folder = Path("./results")
merged_raw_audits_path = data_folder / "merged_raw_audits.parquet"
audits_path = data_folder / "audits.parquet"
enumerators_path = data_folder / "enumerators.parquet"
surveys_path = data_folder / "surveys.parquet"
nodes_path = data_folder / "nodes.parquet"
deindexed_nodes_path = data_folder / "deindexed_nodes.parquet"
questionnaire_path = data_folder / "questionnaire.parquet"
anomaly_results_path = results_folder / "anomaly_results_summary.xlsx"
results_shap_folder = Path("./results/SHAP")
results_enumerator_folder = Path("./results/enumerator")
results_features_folder = Path("./results/feature_comparisons")
